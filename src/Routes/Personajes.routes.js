import {Router}  from 'express'
import {getPersonajes, savePersonajes, updatePersonajes, deletePersonaje} from '../Controllers/personajesController.js'
import { subirImgen } from '../Middleware/Storage.js'
import { verificar } from '../Middleware/Auth.js'

const rutas = Router()
rutas.get('/api/personajes', verificar, getPersonajes)
rutas.get('/api/personajes/id',verificar, getPersonajes)
rutas.post('/api/personajes', verificar, subirImgen.single('imagen'), savePersonajes)
rutas.put('/api/personajes/:id', verificar, subirImgen.single('imagen'), updatePersonajes)
rutas.delete('/api/personajes/:id', verificar, deletePersonaje)

export default rutas
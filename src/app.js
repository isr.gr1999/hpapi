import  express  from "express"
import morgan from "morgan"
import cors from 'cors'
import mongoose from 'mongoose'
import {DB_HOST,DB_DATABASE,DB_PORT} from './config.js'
import rutasPersonajes from './Routes/Personajes.routes.js'
import rutasAuth from "./Routes/Auth.routes.js"

const conexion = 'mongodb://'+DB_HOST+':'+DB_PORT+'/'+DB_DATABASE
mongoose.connect(conexion).then()

const app = express()
app.use(cors({
    methods: 'GET,POST,DELETE,PUT',
    optionsSuccessStatus: 200,
    origin: 'http://localhost:5173',
    credentials: true
}))
app.use(morgan('dev'))
app.use(express.json())
app.use(express.static('public'))
app.use(rutasAuth)
app.use(rutasPersonajes)


//Mandar error si se accede a alguna ruta que no este definida
app.use( (req,res) => {
    res.status(404).json({status:false,errors:'Not found'})
})

//app.get('/', (req,res) => {res.send('Hola mundo')})
export default app
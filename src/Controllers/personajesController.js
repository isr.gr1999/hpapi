import mongoose from "mongoose";
import * as fs from "fs";

const esquema = new mongoose.Schema({
    nombre:String, casa:String ,imagen:String, fecha:Date
},{versionKey:false})

const personajesModel = new mongoose.model('personajes',esquema)

export const getPersonajes = async(req,res) => {
    try{
        const {id} = req.params
        const rows = (id === undefined) ? await personajesModel.find() : await personajesModel.findById(id)
        return res. status(200).json({status:true,data:rows})
    }
    catch(error){
        return res.status(500).json({status:false,errors:[error]})
    }
}

export const updatePersonajes = async(req,res) =>{
    try{
        const {id} = req.params
        const {nombre,casa,fecha} = req.body
        let imagen = ''
        let valores = {nombre:nombre, casa:casa, fecha:fecha}
        if(req.file != null){
            imagen = '/uploads/'+req.file.filename
            valores = {nombre:nombre,casa:casa, fecha:fecha, imagen:imagen}
            await eliminarImagen(id)
        }
        const validacion = validar(nombre,casa,fecha)
        if(validacion == ''){
            await personajesModel.updateOne({_id:id},{$set:valores})
            return res.status(200).json({status:true,message:'Personaje Actualizado'})
        }
        else{
            return res.status(400).json({status:false,errors:validacion})
        }
    }
    catch(error){
        return res.status(500).json({status:false,errors:[error.message]})
    }
}

export const deletePersonaje = async() =>{
    try{
        const {id} = req.params
        await eliminarImagen(id)
        await personajesModel.deleteOne({_id:id})
        return res.status(200),json({status:true,message:'Personaje Eliminado'})
    }
    catch{
        return res.status(500).json({status:false,errors:[error.message]})
    }
}

export const savePersonajes = async(req,res) =>{
    try{
        const {nombre,casa,fecha} = req.body
        const imagen=req.file;
        const validacion = validar(nombre,casa,fecha,imagen,'Y')
        if(validacion == ''){
            const nuevoPersonaje = new personajesModel({
                nombre:nombre,casa:casa,fecha:fecha, imagen:'/uploads/'+req.file.filename
            })
            return await nuevoPersonaje.save().then(
                () => {res.status(200).json({status:true,message:'Personaje Guardado'})}
            )
        }
        else{
            return res.status(400).json({status:false,errors:validacion})
        }
    }
    catch(error){
        return res.status(500).json({status:false,errors:[error.message]})
    }
}


const validar = (nombre, casa, fecha, img, sevalida) => {
    const errors = []
    if (nombre === undefined || nombre.trim() === ''){
        errors.push('El nombre NO debe de estar vacío')
    }
    if (casa === undefined || casa.trim() === ''){
        errors.push('El nombre de la casa a la que pertenece no debe estar vacío')
    }
    if (fecha === undefined || fecha.trim() === '' || isNaN(Date.parse(fecha))){
        errors.push('La fecha de ingreso no debe estar vacía')
    }
    if (sevalida === 'Y' && img === undefined){
        errors.push('Seleccione una imagen en formato png o jpg')
    }
    else{
        if(errors != ''){
            fs.unlinkSync('./public/uploads/'+img.filename)
        }
    }
    return errors
}

const eliminarImagen = async(id) => {
    const personaje = await personajesModel.findById(id)
    const img = personaje.imagen
    fs.unlinkSync('./public/'+img)
}
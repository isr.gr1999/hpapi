import multer from 'multer'

const guardar = multer.diskStorage({
    destination: (req,file,cb) =>{
        //Donde se guardaran las imagenes
        cb(null,'./public/uploads')
    },
    filename: (req,file,cb) =>{
        //Nombre que va a tener esa imagen
        const ext = file.originalname.split('.').pop()
        //La imagen se guardara con la fecha junto con la extension de ese mismo archivo
        cb(null,Date.now()+'.'+ext)
    }
})


//Verificaremmos que los archivos sean de tipo pg
const filtro = (req,file,cb) => {
    if(file && (file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg' || file.mimetype === 'image/png')){
        cb(null,true)
    }else{
        cb(null,false)
    }
}

export const subirImgen = multer({storage:guardar, fileFilter: filtro})